/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.IO;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.v8Metadata;
import ru.shmalevoz.v8cf.storage.Container;
import ru.shmalevoz.v8cf.storage.Entry;
import ru.shmalevoz.v8cf.storage.EntryLink;
import ru.shmalevoz.v8cf.storage.Page;

/**
 * Запись распакованных данных контейнера 1C на диск
 * 
 * @author shmalevoz
 */
public class DataOutput {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(DataOutput.class.getName());	
	
	private final boolean create_root;
	
	/**
	 * Конструктор
	 * @param c Флаг записи корневого элемена дерева объектов
	 */
	public DataOutput(boolean c) {
		create_root = c;
	}
	
	/**
	 * Создает каталог
	 * @param p Родительский каталог
	 * @param n Имя нового каталога
	 * @return Созданный каталог
	 */
	private File createDir(File p, String n) {
		File retval = new File(p, n);
		if (!retval.exists()) {
			retval.mkdir();
		}
		return retval;
	}
	
	/**
	 * Записывает массив байт в файл
	 * 
	 * @param src
	 * @param dst 
	 */
	private void writeByteArray2File(byte[] src, File dst) throws IOException {
		
		FileOutputStream o = new FileOutputStream(dst);
		o.write(src);
		o.close();
	}
	
	private void writeInputStream(InputStream s, File d) throws IOException {
		
		FileOutputStream o = new FileOutputStream(d);
		byte[] buf = new byte[1024];
		int readed = 0;
		
		while ((readed = s.read(buf)) != -1) {
			o.write(buf, 0, readed);
		}
		
		o.close();
	}
	
	private void addEntryDescription(StringBuilder retval, MDEntry e, String prefix) {
		retval.append(prefix).append(e.getName()).append(":").append(e.getUUID()).append("\n");
	}
	
	/**
	 * Записывает объект метаданных
	 * @param e Объект метаданных
	 * @param d Каталог записи
	 * @param obj_uuids Заполняемый список идентификаторов записанных объектов
	 * @param struct Заполняемое текстовое представление записанных объектов
	 * @param prefix Префикс при заполнении текстового представления
	 * @throws IOException 
	 */
	private void writeEntry(MDEntry e, File d, ArrayList<String> obj_uuids, StringBuilder struct, String prefix) throws IOException {
		
		String name = e.getName();
		String uuid = e.getUUID();
		
		addEntryDescription(struct, e, prefix);
		
		//struct.append(prefix).append(name).append(":").append(uuid).append("\n");
		
		if (uuid != null && !uuid.isEmpty()) {
			obj_uuids.add(uuid);
		}
		
		if (e.isDirectory()) {
			// Если подчиненных нет, то и создавать каталог не надо
			if (e.hasEntries()) {
				File entry_dir = new File(d, name);
				if (!entry_dir.exists()) {
					entry_dir.mkdir();
				}

				Iterator<MDEntry> i = e.getEntries();
				while (i.hasNext()) {
					writeEntry(i.next(), entry_dir, obj_uuids, struct, prefix.concat("    "));
				}
			}
		} else {
                           log.log(Level.INFO, "Write {0}", e.getFullName());
			
			// Не все элементы-контейнеры имеют прямое отображение в иерархии конфигурации
			// Но добавить их идентификаторы в обработанные надо. Получать элемент 
			// хранилища необязательно, достаточно разобрать идентификатор
			String[] us = uuid.split("/");
			if (us.length > 1) {
				obj_uuids.add(us[0]);
			}
			
			writeByteArray2File(e.getStorageEntry().getData(), new File(d, name));
		}
		
	}
	
	/**
	 * Выполняет запись метаданных
	 * @param e
	 * @param d
	 * @param obj_uuids 
	 */
	private void writeMetadata(MDEntry e, File d, ArrayList<String> obj_uuids) throws IOException {
		
		StringBuilder struct = new StringBuilder();
		
		File obj_struct_dir;
		
		if (create_root) {
			writeEntry(e, d, obj_uuids, struct, "");
			obj_struct_dir  = new File(d, e.getName());
		} else {
			
			addEntryDescription(struct, e, "");
			String prefix = "    ";
			obj_struct_dir = d;
			
			Iterator<MDEntry> i = e.getEntries();
			while (i.hasNext()) {
				writeEntry(i.next(), d, obj_uuids, struct, prefix);
			}
		}
		
		File r = new File(d, e.getName());
		
		writeInputStream(new ByteArrayInputStream(struct.toString().getBytes(StandardCharsets.UTF_8)), new File(obj_struct_dir, "Структура"));
	}
	
	/**
	 * Запись объектов хранилища, не отображенных на дерево метаданных
	 * @param p Страница хранилища
	 * @param node Лист дерева ссылок на элементы хранилища
	 * @param metadata_uuids Список идентификаторов оттображенных объектов
	 * @param d Каталог записи
	 * @throws IOException 
	 */
	private void writeUnparsed(Page p, Tree<EntryLink> node, ArrayList<String> metadata_uuids, File d) throws IOException {
		
		String key = node.getKey();
		if (key != null && !key.isEmpty() && !node.getValue().hasParent() && !metadata_uuids.contains(key)) {
			Entry e = new Entry(p, node.getValue());
			writeByteArray2File(e.getData(), new File(d, key));
			
			StringBuilder msg = new StringBuilder();
			msg.append("Обнаружен неотображенный элемент ").append(e.getName());
			
			log.severe(msg.toString());
		}
		
		Iterator<Tree<EntryLink>> ch = node.getLeafs();
		while (ch.hasNext()) {
			writeUnparsed(p, ch.next(), metadata_uuids, d);
		}
		
	}
	
	/**
	 * Выполняет запись исходных элементов контейнера в каталог
	 * @param p 
	 * @throws IOException 
	 */
	private void writeSources(Page p, File d) throws IOException {
		
		Iterator<EntryLink> i = p.entriesLinks();
		Entry e;
		
		while (i.hasNext()) {
			
			e = new Entry(p, i.next());
			
			if (e.isContainer()) {
				writeSources(new Container(e.getDataReader(), e).getRootPage(), createDir(d, e.getName()));
			} else {
				writeByteArray2File(e.getData(), new File(d, e.getName()));
			}
		}
	}
	
	/**
	 * Запись содержимого метаданных на диск
	 * @param m Метаданные
	 * @param d Каталог записи
	 * @param createSources Записывать исходные данные хранилища
	 * @param createUnparsed Записывать данные об  необработанных элементах хранилища
	 * @throws IOException 
	 */
	public void write(v8Metadata m, File d, boolean createSources, boolean createUnparsed) throws IOException {
		
		Tree<EntryLink> storage_links = m.getTreeLinks();
		ArrayList<String> metadata_uuids = new ArrayList<>();
		
		// Записываем распарсенную структуру конфигурации
		writeMetadata(m.getRoot(), d, metadata_uuids);
		
		if (createUnparsed) {
			writeUnparsed(m.getContainer().getRootPage(), storage_links, metadata_uuids, createDir(d, "unparsed"));
		}
		if (createSources) {
			writeSources(m.getContainer().getRootPage(), createDir(d, "sources"));
		}
	}
}
