/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

/**
 *
 * @author shmalevoz
 */
public class EntryLink {
	
	private final int attrs_addr;
	private final int data_addr;
	private final EntryLink parent;

	/**
	 * Конструктор
	 * @param a Адрес записи атрибутов 
	 * @param d Адрес записи данных
	 */
	public EntryLink(int a, int d) {
		this(a, d, null);
	}
	
	/**
	 * Конструктор
	 * @param a Адрес записи атрибутов элемента
	 * @param d Адрес записи данных элемента
	 * @param p Ссылка на владельца
	 */
	public EntryLink(int a, int d, EntryLink p) {
		parent = p;
		attrs_addr = a;
		data_addr = d;
	}

	/**
	 * Возвращает смещение записи атрибутов
	 * @return 
	 */
	public int getAttrAddr() {
		return attrs_addr;
	}

	/**
	 * Возвращает смещение записи данных
	 * @return 
	 */
	public int getDataAddr() {
		return data_addr;
	}
	
	/**
	 * Возвращает наличие родительской записи
	 * @return 
	 */
	public boolean hasParent() {
		return parent != null;
	}
	
	/**
	 * Возвращает ссылку на родителя
	 * @return 
	 */
	public EntryLink getParent() {
		return parent;
	}

	/**
	 * Возвращает путь к ссылке 
	 * @return Строка - путь к ссылке
	 */
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		if (hasParent()) {
			s.append(getParent().toString()).append("/");
		}
		s.append(Integer.toHexString(getAttrAddr())).append(":").append(Integer.toHexString(getDataAddr()));
		return s.toString();
	}
	
	public boolean equals(EntryLink l) {
		return (
			hasParent() == l.hasParent()
			&& (hasParent() && getParent().equals(l.getParent()))
			&& getAttrAddr() == l.getAttrAddr() && getDataAddr() == l.getDataAddr()
			);
	}
}
