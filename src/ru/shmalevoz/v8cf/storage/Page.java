/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.logging.Logger;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.IO.RandomAccessible;

/**
 * Страница данных файла cf
 * 
 * @author shmalevoz
 */
public class Page {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(Page.class.getName());
	
	// Размер ссылки на элемент
	private static final int LINK_SIZE = 12;
	// Элемент владелец страницы
	private Container owner;
	// Адрес начальной записи страницы
	private int start_addr;
	
	/**
	 * Класс обхода списка элементов
	 */
	private class EntriesIterator implements Iterator<EntryLink> {
		
		private final Record record;
		private int index = 0;
		private int count = 0;
		private byte[] buffer;

		/**
		 * Выполняет заполнение буфера очередными данными страницы
		 * 
		 * @throws IOException 
		 */
		private void read() throws IOException {

			ByteArrayOutputStream o = new ByteArrayOutputStream();
			o.write(record.getBody());

			while (o.size() % LINK_SIZE != 0 && record.hasNext()) {
				record.next();
				o.write(record.getBody());
			}
			o.close();
			buffer = o.toByteArray();
			count = o.size() / LINK_SIZE;

			// Для последней записи надо скорректировать число элементов. В незначащих запясях нули
			if (!record.hasNext()) {

				int i = 0;
				while (ru.shmalevoz.utils.Conversion.ByteArray2Int(
					Arrays.copyOfRange(buffer, i * LINK_SIZE, (i + 1) * LINK_SIZE - 4)
					, 0, true) != 0
					&& i < count) {

					i++;
				}

				count = i;
			}
		}
		
		/**
		 * Конструктор
		 * @param r 
		 */
		EntriesIterator(Page p, int offset) throws IOException {
			record = new Record(p.getSource(), offset);
			read();
		}
		
		@Override
		public boolean hasNext() {
			return index < count;
		}

		@Override
		public EntryLink next() {
			
			log.info("Возвращаем элемент " + index + " записи страницы " + Integer.toHexString(record.getAddr()));

			byte[] d = Arrays.copyOfRange(buffer, index * LINK_SIZE, (index + 1) * LINK_SIZE - 4);
			EntryLink link = new EntryLink(
				ru.shmalevoz.utils.Conversion.ByteArray2Int(d, 0, true)
				, ru.shmalevoz.utils.Conversion.ByteArray2Int(d, 4, true));

			index++;
			if (index >= count && record.hasNext()) {
				record.next();
				try {
					read();
				} catch (IOException e) {
					throw new UnsupportedOperationException(e.getMessage());
				}
				
				index = 0;
			}

			return link;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
	}
	
	/**
	 * Конструктор
	 * @param c
	 * @param offset 
	 * @throws java.io.IOException 
	 */
	public Page(Container c, int offset) throws IOException {
		owner = c;
		start_addr = offset;
	}
	
	/**
	 * Возвращает контейнер-владелец
	 * @return 
	 */
	public Container getOwnerContainer() {
		return owner;
	}
	
	/**
	 * Возвращает источник данных
	 * @return 
	 */
	public RandomAccessible getSource() {
		return getOwnerContainer().getSource();
	}
	
	/**
	 * 
	 * @return
	 * @throws IOException 
	 */
	public Iterator<EntryLink> entriesLinks() throws IOException {
		return new EntriesIterator(this, start_addr);
	}
	
	/**
	 * Возвращает карту именованных ссылок на элементв страницы
	 * @return 
	 */
	public LinkedHashMap<String, EntryLink> getNamedLinks() throws IOException {
		
		LinkedHashMap<String, EntryLink> result = new LinkedHashMap<>();
		Entry e;
		EntryLink l;
		Iterator<EntryLink> i = entriesLinks();
		
		while (i.hasNext()) {
			l = i.next();
			e = new Entry(this, l);
			result.put(e.getName(), l);
		}
		
		return result;
	}
	
	/**
	 * Заполняет дерево ссылками
	 * @param tree
	 * @param p 
	 */
	private void fillTreeLinks(Tree<EntryLink> tree, Page p) throws IOException {
		
		Iterator<EntryLink> i = p.entriesLinks();
		Entry e;
		
		while (i.hasNext()) {
			e = new Entry(p, i.next());
			Tree<EntryLink> n = new Tree<>(e.getLink(), e.getName(), tree);
			tree.add(n);
			if (e.isContainer()) {
				fillTreeLinks(n, new Container(e.getDataReader(), e).getRootPage());
			}
		}
	}
	
	/**
	 * Возвращает дерево ссылок на элементы хранилища. Ключ дерева - идентификатор элемента в хранилище.
	 * @return Дерево ссылок на элементы хранилища
	 * @throws java.io.IOException 
	 */
	public Tree<EntryLink> getTreeLinks() throws IOException {
		
		Tree<EntryLink> root = new Tree<>();
		
		fillTreeLinks(root, this);
		
		return root;
	}
}
