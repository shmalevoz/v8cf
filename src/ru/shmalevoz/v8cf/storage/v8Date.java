/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

import java.math.BigInteger;

/**
 * Дата время 1Сv8. Начальная дата отсчета с 01.01.0001 00:00:00
 * 
 * @author shmalevoz
 */
public class v8Date extends java.util.GregorianCalendar {
	
	public static final java.math.BigInteger offset = new BigInteger("-62135596800000");
	
	
		
		
		
	
}
