/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.storage;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author shmalevoz
 */
public class BufferedEntryReader {
	
	private final int BUFFER_SIZE = 4;
	private final ArrayList<String> keys;
	private final ArrayList<Entry> values;
	private final Page root;
	
	private int index;
	
	public BufferedEntryReader(Page r) {
		
		root = r;
		index = 0;
		
		keys = new ArrayList<>();
		values = new ArrayList<>();
		
		for (int i = 0; i < BUFFER_SIZE; i++) {
			keys.add(null);
			values.add(null);
		}
	}
	
	private void fillHierarchyList(ArrayList<EntryLink> h, EntryLink l) {
		if (l.hasParent()) {
			fillHierarchyList(h, l.getParent());
		}
		h.add(l);
	}
	
	public Entry read(EntryLink link) throws IOException {
		
		String key = link.toString();
		int key_index = keys.indexOf(key);
		
		// Содержит элемент
		if (key_index != -1) {
			return values.get(key_index);
		}
		
		ArrayList<EntryLink> h = new ArrayList<>();
		fillHierarchyList(h, link);
		
		Entry retval = null;
		Page p = root;
		for (EntryLink e : h) {
			retval = new Entry(p, e);
			if (retval.isContainer()) {
				p = new Container(retval.getDataReader(), retval).getRootPage();
			}
		}
		
		index++;
		if (index == BUFFER_SIZE) {
			index = 0;
		}
		
		keys.remove(index);
		values.remove(index);
		
		keys.add(index, key);
		values.add(index, retval);
		
		return retval;
	}
}
