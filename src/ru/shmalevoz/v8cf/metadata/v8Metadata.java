/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata;

import java.io.File;
import ru.shmalevoz.v8cf.metadata.parsers.DictionaryParser;
import java.io.IOException;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.parsers.sections.AccountingRegisters;
import ru.shmalevoz.v8cf.metadata.parsers.sections.Configuration;
import ru.shmalevoz.v8cf.metadata.parsers.sections.ExternalDataProcessor;
import ru.shmalevoz.v8cf.metadata.parsers.sections.ExternalReport;
import ru.shmalevoz.v8cf.storage.Container;
import ru.shmalevoz.v8cf.storage.EntryLink;
import ru.shmalevoz.v8cf.storage.Page;

/**
 * Структура метаданных 1С Предприятие 8.х
 * @author shmalevoz
 */
public class v8Metadata {
	
	private final MDEntry root;
	private final Tree<EntryLink> links_tree;
	private final Container storage;
	
	/**
	 * Конструктор
	 * @param f Декомпилируемый файл
	 * @throws IOException 
	 */
	public v8Metadata(File f) throws IOException {
		
		// Корневая страница хранилиища
		storage = new Container(f);
		Page root_page = storage.getRootPage();
		// Дерево ссылок на элементы хранилища. Будет выполен обход всего хранилища
		links_tree = root_page.getTreeLinks();
		
		// В root хранится идентиификатор элемента с основным словарем метаданных
		Tree<String> dict = DictionaryParser.parse("root", root_page, links_tree);
		dict = DictionaryParser.parse(DictionaryParser.getElement(dict, "0/0/1"), root_page, links_tree);
		
		// По словарю определяем что именно содержится в контейнере 
		// конфигурация, внешний отчет или внешняя обработка
		if (dict.get(new AccountingRegisters().getSectionUUID(), true) != null) {
			// Проверяем на примере регистров бухгалтерии. во внешних их не бывает
			root = new MDEntry("Конфигурация", new Configuration(), dict, links_tree, root_page);
		} else if (dict.getLeafAt(0).getLeafAt(3).getLeafAt(1).getLeafAt(1).getLeafsCount() > 10) {
			// у отчета словарь больше чем у обработки
			root = new MDEntry("ВнешнийОтчет", new ExternalReport(), dict, links_tree, root_page);
		} else {
			root = new MDEntry("ВнешняяОбработка", new ExternalDataProcessor(), dict, links_tree, root_page);
		}
	}
	
	/**
	 * Конструктор
	 * @param f Расположение декомпилируемого файла
	 * @throws IOException 
	 */
	public v8Metadata(String f) throws IOException {
		this(new File(f));
	}
	
	/**
	 * Возвращает корневую секцию конфигурации
	 * @return 
	 */
	public MDEntry getRoot() {
		return root;
	}
	
	/**
	 * Возвращает дерево ссылок на элементы хранилища
	 * @return 
	 */
	public Tree<EntryLink> getTreeLinks() {
		return links_tree;
	}
	
	/**
	 * Возвращает корневой контейнер метаданных
	 * @return 
	 */
	public Container getContainer() {
		return storage;
	}
	
}
