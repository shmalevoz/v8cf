/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import ru.shmalevoz.v8cf.metadata.MDElement;
import java.io.IOException;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;

/**
 *
 * @author shmalevoz
 */
public interface EntryParser {
	
	/**
	 * Возвращает наличие идентификатора секции
	 * @return 
	 */
	public boolean hasSectionUUID();
	
	/**
	 * Возвращает идентификатор секции
	 * @return 
	 */
	public String getSectionUUID();
	
	/**
	 * Возвращает строку пути к имени объекта в его словаре
	 * @return 
	 */
	public String getObjectNamePath();
	
	/**
	 * Возвращает список ссылок подчиненных элементов
	 * @param e Элемент-родитель
	 * @return
	 * @throws IOException 
	 */
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException;
	
	/**
	 * Возвращает список возможных элементов объекта метаданных
	 * @return 
	 */
	public List<MDElement> listElements();
}
