/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Forms;
import ru.shmalevoz.v8cf.metadata.parsers.sections.Commands;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class Tasks extends SectionParser {
	
	//private static final String name = "Задачи";
	// e97c0570-251c-4566-b0f1-10686820f143 Реквизиты адресации
	// 8ddfb495-c5fc-46b9-bdc5-bcf58341bff0 Реквизиты
	// ee865d4b-a458-48a0-b38f-5a26898feeb0 Табличные части
	
	private static final String UUID = "3e63355c-1378-4953-be9b-1deb5fb6bec5";
	private static final String PATH = "0/0/1/1/2";
	private static final String FORMS_UUID = "3f58cbfb-4172-4e54-be49-561a579bb38b"; // 0/0/4/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/2";
	private static final String COMMANDS_UUID = "f27c2152-a2c9-4c30-adb1-130f5eb2590f"; // Путь 0/0/8/0
	

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".5", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".6", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".7", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
