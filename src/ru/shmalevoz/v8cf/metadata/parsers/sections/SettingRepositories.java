/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Forms;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class SettingRepositories extends SectionParser {
	
	//private static final String name = "ХранилищаНастроек";
	private static final String UUID = "46b4cd97-fd13-4eaa-aba2-3bddd7699218";
	private static final String PATH = "0/0/1/1/1/2";
	private static final String FORMS_UUID = "b8533c0c-2342-4db3-91a2-c2b08cbf6b23"; // Путь к ид 0/0/4/0
	// private static final String TEMPLATES_UUID = "3daea016-69b7-4ed4-9453-127911372fe6"; // Путь к ид 0/0/3/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("МодульМенеджера", ".8", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
