/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class ChartsOfAccounts extends SectionParser {
	
	//private static final String name = "ПланыСчетов";
	// 6e65cbf5-daa8-4d8d-bef8-59723f4e5777 Реквизиты
	// 4c7fec95-d1bd-4508-8a01-f1db090d9af8 Табличные части
	// 78bd1243-c4df-46c3-8138-e147465cb9a4 Признаки учета
	// c70ca527-5042-4cad-a315-dcb4007e32a3 Признаки учета субконто
	
	private static final String UUID = "238e7e88-3c5f-48b2-8a3b-81ebbecb20ed";
	private static final String PATH = "0/0/1/15/1/2";
	private static final String FORMS_UUID = "5372e285-03db-4f8c-8565-fe56f1aea40e"; // 0/0/6/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/2";
	private static final String COMMANDS_UUID = "0df30176-6865-4787-9fc8-609eb144174f"; // Путь 0/0/3/0
	//private static final String COMMANDS_NAME_PATH = "0/0/1/2/9/2";
	//private static final String COMMANDS_UUID_PATH = "0/0/1/1/1";
	

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".5", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("ПредопределенныеЭлементы", ".9", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".14", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".15", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID))); // ,COMMANDS_NAME_PATH, COMMANDS_UUID_PATH)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
