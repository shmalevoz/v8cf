/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Forms;
import ru.shmalevoz.v8cf.metadata.parsers.sections.Commands;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class Reports extends SectionParser {
	
	//private static final String name = "Отчеты";
	// 7e7123e0-29e2-11d6-a3c7-0050bae0a776 Реквизиты
	// b077d780-29e2-11d6-a3c7-0050bae0a776 ТабличныеЧасти
	
	private static final String UUID = "631b75a0-29e2-11d6-a3c7-0050bae0a776";
	private static final String PATH = "0/0/1/3/1/2";
	private static final String FORMS_UUID = "a3b368c0-29e2-11d6-a3c7-0050bae0a776"; // 0/0/5/0
	private static final String FORMS_NAME_PATH = "0/0/1/1/1/1/2";
	private static final String COMMANDS_UUID = "e7ff38c0-ec3c-47a0-ae90-20c73ca72246"; // Путь 0/0/7/0
	private static final String COMMANDS_NAME_PATH = "0/0/1/2/9/2";
	private static final String COMMANDS_UUID_PATH = "0/0/1/1/1";

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".1", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".0", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".2", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID, FORMS_NAME_PATH)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID, COMMANDS_NAME_PATH, COMMANDS_UUID_PATH)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
