/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.io.IOException;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.AbstractParser;
import ru.shmalevoz.v8cf.metadata.parsers.DictionaryParser;
import ru.shmalevoz.v8cf.metadata.parsers.EmptyParser;
import ru.shmalevoz.v8cf.storage.Entry;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 * Конфигурация поставщика
 * @author shmalevoz@gmail.com (Valeriy Krynin)
 */
public class ProviderConfiguration extends AbstractParser {
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		Tree<String> dict = e.getDictionary();
		
		Tree<EntryLink> links = e.getStorageLinks();
		EmptyParser parser = new EmptyParser();
		
		// Словарь данных
		Tree<String> root_dict;
		try {
			root_dict = DictionaryParser.parse("root", e.getStoragePage(), links);
		} catch (IOException ex) {
			throw new UnsupportedOperationException("Не удалось получить элемент словаря конфигурации!");
		}
		// Идентификатор словаря конфигурации
		String dict_uuid = DictionaryParser.getElement(root_dict, "0/0/1");
		// Идентиификатор конфигурации
		String base_uuid = DictionaryParser.getElement(dict, "0/0/3/1/1/1/1/1/2");
		
		String provider_uuid = base_uuid.concat(".4");
		Tree<EntryLink> pl = links.get(provider_uuid, true);
		
		if (pl != null) {
			// Структура поставки конфгурации
			super.addEntryLinkElement(retval
				, "Структура"
				, new EmptyParser()
				, e
				, provider_uuid
				, links);
			
			Entry storage_entry;
			Tree<String> provider_dict;
			try {
				storage_entry = e.getStorageEntryReader().read(pl.getValue());
				provider_dict = DictionaryParser.parse(storage_entry.getDataInputStream());
			} catch (IOException ex) {
				throw new UnsupportedOperationException("Не удалось получить элемент словаря конфигурации поставщика!");
			}
			
			String provider_data_uuid = dict_uuid.concat(".")
				.concat(DictionaryParser.getElement(provider_dict, "0/0/3"));
			
			if (links.get(provider_data_uuid, true) != null) {
				super.addEntryLinkElement(retval
					, "Данные"
					, new EmptyParser()
					, e
					, provider_data_uuid
					, links);
			}
		}
		
		return retval;
	}
	
	
}
