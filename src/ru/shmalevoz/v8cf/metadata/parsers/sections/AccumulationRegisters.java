/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class AccumulationRegisters extends SectionParser {
	
	//private static final String name = "РегистрыНакопления";
	// b64d9a43-1642-11d6-a3c7-0050bae0a776 Измерения
	// b64d9a41-1642-11d6-a3c7-0050bae0a776 Ресурсы
	// b64d9a42-1642-11d6-a3c7-0050bae0a776 Реквизиты
	
	private static final String UUID = "b64d9a40-1642-11d6-a3c7-0050bae0a776";
	private static final String PATH = "0/0/1/13/1/2";
	private static final String FORMS_UUID = "b64d9a44-1642-11d6-a3c7-0050bae0a776"; // 0/0/8/0
	private static final String COMMANDS_UUID = "99f328af-a77f-4572-a2d8-80ed20c81890"; // Путь 0/0/4/0
	private static final String COMMANDS_NAME_PATH = "0/0/1/2/9/2";
	private static final String COMMANDS_UUID_PATH = "0/0/1/1/1";
	

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".0", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульНабораЗаписей", ".1", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".2", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Агрегаты", ".3", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID,COMMANDS_NAME_PATH, COMMANDS_UUID_PATH)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
