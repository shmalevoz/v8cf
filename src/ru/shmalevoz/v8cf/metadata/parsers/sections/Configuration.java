/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.parsers.AbstractParser;

/**
 *
 * @author shmalevoz
 */
public class Configuration extends AbstractParser {

	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		Tree<String> dict = e.getDictionary();
		
		super.addEntryLinkSubsection(retval, "Общие", new Common(), e, dict);
		super.addEntryLinkSubsection(retval, "Константы", new Constants(), e, dict);
		super.addEntryLinkSubsection(retval, "Справочники", new Catalogs(), e, dict);
		super.addEntryLinkSubsection(retval, "Документы", new Documents(), e, dict);
		super.addEntryLinkSubsection(retval, "ЖурналыДокументов", new DocumentJournals(), e, dict);
		super.addEntryLinkSubsection(retval, "Перечисления", new Enumerations(), e, dict);
		super.addEntryLinkSubsection(retval, "Отчеты", new Reports(), e, dict);
		super.addEntryLinkSubsection(retval, "Обработки", new DataProcessors(), e, dict);
		super.addEntryLinkSubsection(retval, "ПланыВидовХарактеристик", new ChartsOfCharacteristicTypes(), e, dict);
		super.addEntryLinkSubsection(retval, "ПланыСчетов", new ChartsOfAccounts(), e, dict);
		super.addEntryLinkSubsection(retval, "ПланыВидовРасчетов", new ChartsOfCalculationTypes(), e, dict);
		super.addEntryLinkSubsection(retval, "РегистрыСведений", new InformationRegisters(), e, dict);
		super.addEntryLinkSubsection(retval, "РегистрыНакопления", new AccumulationRegisters(), e, dict);
		super.addEntryLinkSubsection(retval, "РегистрыБухгалтерии", new AccountingRegisters(), e, dict);
		super.addEntryLinkSubsection(retval, "РегистрыРасчетов", new CalculationRegisters(), e, dict);
		super.addEntryLinkSubsection(retval, "БизнесПроцессы", new BusinessProcesses(), e, dict);
		super.addEntryLinkSubsection(retval, "Задачи", new Tasks(), e, dict);
		super.addEntryLinkSubsection(retval, "ВнешниеИсточникиДанных", new ExternalDataSources(), e, dict);
		
		super.addEntryLinkElement(retval, "Свойства", new ConfigurationProperties(), e, null);
		
		return retval;
	}
	
}
