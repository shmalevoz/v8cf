/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class Catalogs extends SectionParser {
	
	//private static final String name = "Справочники";
	private static final String UUID = "cf4abea6-37b2-11d4-940f-008048da11f9";
	private static final String PATH = "0/0/1/9/1/2";
	private static final String FORMS_UUID = "fdf816d2-1ead-11d5-b975-0050bae0a95d"; // Путь 0/0/7/0
	private static final String COMMANDS_UUID = "4fe87c89-9ad4-43f6-9fdb-9dc83b3879c6"; // Путь 0/0/4/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".1", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("ПредопределенныеЭлементы", ".1c", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("МодульОбъекта", ".0", MDElement.MODULE));
		ELEMENTS.add(new MDElement("МодульМенеджера", ".3", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
		ELEMENTS.add(new MDElement("Макеты", "", MDElement.SUBSECTION, new Templates()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
