/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SubsectionParser;

/**
 *
 * @author shmalevoz
 */
public class Forms extends SubsectionParser {

	//private static final String name = "Формы";
	private final String UUID;
	private final String PATH;
	private final Tree<String> dictionary;
	
	private static final String STANDARD_PATH = "0/0/1/1/1/2";
	
	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("Форма", ".0", MDElement.FORM));
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".1", MDElement.FINAL_DATA));
	}
	
	public Forms(String u) {
		this(u, STANDARD_PATH);
	}
	
	public Forms(String u, String p) {
		this(u, p, null);
	}
	
	public Forms(String u, Tree<String> d) {
		this(u, STANDARD_PATH, d);
	}
	
	public Forms(String u, String p, Tree<String> d) {
		UUID = u;
		PATH = p;
		dictionary = d;
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		if (dictionary == null) {
			return super.listEntriesLinks(e);
		}
		return super.listEntriesLinksFromDict(e, dictionary);
	}
}
