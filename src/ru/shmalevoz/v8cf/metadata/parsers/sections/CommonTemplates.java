/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class CommonTemplates extends SectionParser {
	
	//private static final String name = "ОбщиеМакеты";
	private static final String UUID = "0c89c792-16c3-11d5-b96b-0050bae0a95d";
	private static final String PATH = "0/0/1/1/2";

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("Данные", ".0", MDElement.FINAL_DATA));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
