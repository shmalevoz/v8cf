/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SubsectionParser;

/**
 *
 * @author shmalevoz
 */
public class SubordinateSubsystem extends SubsectionParser {

	//private static final String name = "ПодчиненныеПодсистемы";
	private static final String UUID = "37f2fa9a-b276-11d4-9435-004095e12fc7";
	private static final String PATH = "0/0/1/1/2";
	
	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("СправочнаяИнформация", ".0", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("КомандныйИнтерфейс", ".1", MDElement.FINAL_DATA));
		ELEMENTS.add(new MDElement("Подчиненные", "", MDElement.SUBSECTION, new SubordinateSubsystem()));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
