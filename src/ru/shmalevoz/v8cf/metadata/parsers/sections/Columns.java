/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.EmbeddedSectionParser;

/**
 *
 * @author shmalevoz
 */
public class Columns extends EmbeddedSectionParser {
	
	// Графы журнала документов
	private static final String UUID = "5aee69df-0513-4c6c-9815-103102471712";
	
	private static final String NAME_PATH = "0/0/1/2";
	private static final String UUID_PATH = "0/0/1/1/2";

	private static final List<MDElement> ELEMENTS = new ArrayList<>();

	public Columns() {
		super("", MDElement.FINAL_DATA);
	}

	@Override
	public String getObjectNamePath() {
		return NAME_PATH;
	}
	
	@Override
	public String getObjectUUIDPath() {
		return UUID_PATH;
	}
	
	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}

	@Override
	public boolean hasStorageEntry() {
		return false;
	}
}
