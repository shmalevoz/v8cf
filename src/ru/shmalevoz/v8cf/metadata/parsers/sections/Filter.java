/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers.sections;

import ru.shmalevoz.v8cf.metadata.parsers.sections.Commands;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.v8cf.metadata.MDElement;
import ru.shmalevoz.v8cf.metadata.parsers.SectionParser;

/**
 *
 * @author shmalevoz
 */
public class Filter extends SectionParser {
	
	//private static final String name = "КритерииОтбора";
	private static final String UUID = "3e7bfcc0-067d-11d6-a3c7-0050bae0a776";
	private static final String PATH = "0/0/1/5/1/2";
	private static final String FORMS_UUID = "00867c40-06b1-11d6-a3c7-0050bae0a776"; // Путь к ид 0/0/3/0
	private static final String COMMANDS_UUID = "23fa3b84-220a-40e9-8331-e588bed87f7d"; // Путь к ид 0/0/4/0

	private static final List<MDElement> ELEMENTS = new ArrayList<>();
	static {
		ELEMENTS.add(new MDElement("МодульМенеджера", ".0", MDElement.MODULE));
		ELEMENTS.add(new MDElement("Формы", "", MDElement.SUBSECTION, new Forms(FORMS_UUID)));
		ELEMENTS.add(new MDElement("Команды", "", MDElement.SUBSECTION, new Commands(COMMANDS_UUID)));
	}

	@Override
	public String getSectionUUID() {
		return UUID;
	}

	@Override
	public String getObjectNamePath() {
		return PATH;
	}
	
	@Override
	public List<MDElement> listElements() {
		return ELEMENTS;
	}
}
