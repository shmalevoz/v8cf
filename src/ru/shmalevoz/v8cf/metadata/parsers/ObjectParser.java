/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import ru.shmalevoz.v8cf.metadata.MDElement;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 *
 * @author shmalevoz
 */
public class ObjectParser extends AbstractParser {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(ObjectParser.class.getName());
	
	private final String name_path;
	private final List<MDElement> elements;
	
	public ObjectParser(String p, List<MDElement> e) {
		name_path = p;
		elements = e;
	}
	
	@Override
	public String getObjectNamePath() {
		return name_path;
	}
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		
		EmptyParser p = new EmptyParser();
		
		try {
			retval.add(new MDEntryLink("Структура", p, e, e.getStorageEntry().getLink(), false));
		} catch (IOException ex) {
			log.severe(ex.getMessage());
		}
		
		/**
		 * Добавляем элементы, объявленные как возможно присутствующие в парсере
		 */
		
		String base_uuid = e.getUUID();
		Tree<EntryLink> links = e.getStorageLinks();
		Tree<EntryLink> node;
		String uuid;
		
		EmptyParser empty_parser = new EmptyParser();
		Tree<String> dictionary = e.getDictionary();
		
		for (MDElement elem : elements) {
			uuid = base_uuid.concat(elem.getPostfix());
			node = links.get(uuid, true);
			if (node != null) {
				
				switch (elem.getType()) {
					case MDElement.FINAL_DATA: {
						super.addEntryLinkElement(retval, elem.getName(), new EmptyParser(), e, node.getValue());
						break;
					}
					case MDElement.SUBSECTION: {
						super.addEntryLinkSubsection(retval, elem.getName(), elem.getParser(), e, dictionary);
						break;
					}
					case MDElement.MODULE: {
						super.addEntryLinkModule(
							retval
							, elem.getName()
							, uuid
							, e
							, links);
						break;
					}
					case MDElement.FORM: {
						super.addEntryLinkForm(
							retval
							, elem.getName()
							, uuid
							, e
							, links);
						break;
					}
					case MDElement.WSDL: {
						super.addEntryLinkWSDL(
							retval
							, elem.getName()
							, uuid
							, e
							, links);
						break;
					}
				}
			}
		}
		
		return retval;
	}
}
