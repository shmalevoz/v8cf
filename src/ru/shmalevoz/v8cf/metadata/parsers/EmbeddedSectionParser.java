/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import ru.shmalevoz.v8cf.metadata.MDElement;
import java.io.IOException;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 * Класс разбора вложенной секции - команды, графы журналов документов,...
 * @author shmalevoz
 */
public abstract class EmbeddedSectionParser extends SubsectionParser {
	
	private final String postfix;
	private final int type;
	
	public EmbeddedSectionParser(String p, int t) {
		postfix = p;
		type = t;
	}
	
	/**
	 * Возвращает путь к идентифкатору объекта во вложенной секции
	 * @return 
	 */
	public abstract String getObjectUUIDPath();
	
	/**
	 * Возвращает наличие отображения элемента в хранище
	 * @return 
	 */
	public abstract boolean hasStorageEntry();
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		
		List<Tree<String>> sect_leafs = super.listSectionLeafs(
			DictionaryParser.parse(e.getParent().getStorageEntry().getDataInputStream())
			, getSectionUUID());
		Tree<EntryLink> storage_links = e.getStorageLinks();
		
		String name;
		String uuid;
		
		for (Tree<String> leaf : sect_leafs) {
			
			name = DictionaryParser.getElement(leaf, getObjectNamePath());
			uuid = DictionaryParser.getElement(leaf, getObjectUUIDPath()).concat(postfix);
			
			if (hasStorageEntry()) { // имеет отображение в хранилище
				if (type == MDElement.MODULE) {
					super.addEntryLinkModule(retval
						, name
						, uuid
						, e
						, storage_links);
				}
			} else { // отображения в хранилище нет. добавляем ссылку на родительские данные как данные элемента
				retval.add(new MDEntryLink(name, new EmptyParser(), e, e.getParent().getStorageLink(), false));
			}
		}
		
		return retval;
	}
	
	
}
