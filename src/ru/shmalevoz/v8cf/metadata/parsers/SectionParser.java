/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata.parsers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.MDEntry;
import ru.shmalevoz.v8cf.metadata.MDEntryLink;
import ru.shmalevoz.v8cf.storage.EntryLink;

/**
 *
 * @author shmalevoz
 */
public abstract class SectionParser extends AbstractParser {
	
	protected List<Tree<String>> listSectionLeafs(Tree<String> root, String u) {
		
		ArrayList<Tree<String>> retval = new ArrayList<>();
		
		Tree<String> sect_root = root.get(u, true).getParent();
		int len = sect_root.getLeafsCount();
		for (int i = 2; i < len; i++) {
			retval.add(sect_root.getLeafAt(i));
		}
		
		return retval;
	}
	
	protected List<MDEntryLink> listEntriesLinksFromDict(MDEntry e, Tree<String> dict) {
		
		List<MDEntryLink> retval = super.createMDEntryLinkList();
		Tree<EntryLink> links = e.getStorageLinks();
		ObjectParser parser = new ObjectParser(getObjectNamePath(), listElements());
		
		for (Tree<String> leaf : listSectionLeafs(dict, getSectionUUID())) {
			retval.add(new MDEntryLink(parser
				, e
				, links.get(leaf.getValue(), true).getValue() // Ссылка получена по идентификатору из словаря
				, true));
		}
		
		return retval;
	}
	
	@Override
	public List<MDEntryLink> listEntriesLinks(MDEntry e) throws IOException {
		
		return listEntriesLinksFromDict(e, e.getDictionary());
		
	}
}
