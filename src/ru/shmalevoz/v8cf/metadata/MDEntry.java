/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf.metadata;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import ru.shmalevoz.utils.collection.Tree;
import ru.shmalevoz.v8cf.metadata.parsers.DictionaryParser;
import ru.shmalevoz.v8cf.metadata.parsers.EntryParser;
import ru.shmalevoz.v8cf.storage.BufferedEntryReader;
import ru.shmalevoz.v8cf.storage.Entry;
import ru.shmalevoz.v8cf.storage.EntryLink;
import ru.shmalevoz.v8cf.storage.Page;

/**
 * Объект метаданных
 * @author shmalevoz
 */
public class MDEntry {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(MDEntry.class.getName());
	
	private String name;
	private String uuid;
	private MDEntry parent;
	private List<MDEntryLink> entries;
	private final EntryParser parser;
	
	// Ссылка на элемент хранилища
	private final EntryLink storage_link;
	private final boolean is_folder;
	
	// Ссылки на объекты хранилища
	private final Tree<String> dictionary; // Основной словарь
	private final Tree<EntryLink> storage_links; // Полное дерево ссылок
	private final Page storage_page; // Корневая страница хранилища
	
	private final BufferedEntryReader entry_reader; // Буферизованное чтение элементов хранилища
	
	/**
	 * Класс перечисления подчиненных элементов
	 */
	private class MDEntriesIterator implements Iterator<MDEntry> {
		
		private int index;
		private final List<MDEntryLink> entries;

		public MDEntriesIterator(List<MDEntryLink> l) {
			this.index = 0;
			this.entries = l;
		}

		@Override
		public boolean hasNext() {
			return this.index < this.entries.size();
		}

		@Override
		public MDEntry next() {
			this.index++;
			MDEntry retval = null;
			try {
				retval = this.entries.get(index - 1).createEntry();
			} catch (IOException ex) {
				log.severe(ex.getMessage());
			}
			return retval;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
	}
	
	/**
	 * Конструктор корневого элемента
	 * @param n Имя
	 * @param e Парсер элемента
	 * @param d Корневой словарь метаданных
	 * @param l Дерево ссылок на элементы хранилища
	 * @param p Корневая страница хранилища
	 */
	public MDEntry(String n, EntryParser e,  Tree<String> d, Tree<EntryLink> l, Page p) {
		
		name = n;
		uuid = null;
		parent = null;
		storage_link = null;
		
		parser = e;
		dictionary = d;
		storage_links = l;
		storage_page = p;
		entry_reader = new BufferedEntryReader(p);
		
		is_folder = true;
		entries = null;
	}

	
	/**
	 * Конструктор подчиненного элемента абстракной группы
	 * @param n Имя 
	 * @param p Родитель
	 * @param e
	 * @throws java.io.IOException
	 */
	public MDEntry(String n, EntryParser p, MDEntry e) throws IOException {
		name = n.replaceAll("\"", "");
		uuid = null;
		parent = e;
		parser = p;
		is_folder = true;
		
		storage_link = null;
		storage_links = null;
		storage_page = null;
		dictionary = null;
		entry_reader = null;
		entries = null;
	}
	
	/**
	 * Конструктор элемента метаданных с отображением в хранилище
	 * @param p
	 * @param e
	 * @param l
	 * @param is_dir
	 * @throws IOException 
	 */
	public MDEntry(EntryParser p, MDEntry e, EntryLink l, boolean is_dir) throws IOException {
		storage_link = l;
		parent = e;
		parser = p;
		
		Entry s_entry = getStorageEntry();
		uuid = s_entry.getFullName();
		
		name = DictionaryParser.getElement(DictionaryParser.parse(s_entry.getDataInputStream())
			, parser.getObjectNamePath())
			.replaceAll("\"", "");
		
		is_folder = is_dir;
		
		storage_links = null;
		storage_page = null;
		dictionary = null;
		entry_reader = null;
		entries = null;
	}
	
	/**
	 * Конструктор элемента метаданных, не имеющего имени в хранилище
	 * @param n
	 * @param p
	 * @param e
	 * @param l
	 * @param is_dir
	 * @throws IOException 
	 */
	public MDEntry(String n, EntryParser p, MDEntry e, EntryLink l, boolean is_dir) throws IOException {
		
		storage_link = l;
		parent = e;
		parser = p;
		name = n.replaceAll("\"", "");
		
		uuid = getStorageEntry().getFullName();
		
		is_folder = is_dir;
		
		storage_links = null;
		storage_page = null;
		dictionary = null;
		entry_reader = null;
		entries = null;
	}
	
	/**
	 * Возвращает имя объекта
	 * @return 
	 */
	public final String getName() {
		return name;
	}
	
	/**
	 * Возвращает полное имя объекта
	 * @return Строка, полное имя
	 */
	public final String getFullName() {
		StringBuilder retval = new StringBuilder();
		if (hasParent()) {
			retval.append(getParent().getFullName()).append(".");
		}
		retval.append(getName());
		return retval.toString();
	}
	
	/**
	 * Устанавливает имя объекта
	 * @param n Новое имя
	 */
	public final void setName(String n) {
		name = n;
	}

	/**
	 * Возвращает уникальный идентификатор объекта
	 * @return Строка
	 */
	public final String getUUID() {
		String retval = uuid;
		if (retval == null && parser != null) {
			retval = parser.getSectionUUID();
		}
		return retval;
	}
	
	/**
	 * Устанавливает уникальный идентификатор объекта
	 * @param u Новый идентификатор
	 */
	public final void setUUID(String u) {
		uuid = u;
	}
	
	private void readEntries() {
		if (entries == null) {
			try {
				entries = parser.listEntriesLinks(this);
			} catch (Exception ex) {

				StringBuilder err = new StringBuilder("Не удалось получить список подчиненных для ");
				err.append(getFullName());
				err.append(": ").append(ex.getMessage());

				log.severe(err.toString());
				entries = new ArrayList<>();
			}
		}
	}
	
	/**
	 * Возвращает наличие подчиненных элементов
	 * @return 
	 */
	public boolean hasEntries() {
		readEntries();
		return !entries.isEmpty();
	}
	
	/**
	 * Возвращает перечисление подчиненных объектов
	 * @return 
	 */
	public Iterator<MDEntry> getEntries() {
		readEntries();
		return new MDEntriesIterator(entries);
	}
	
	/**
	 * Возвращает признак корневого элемента
	 * @return 
	 */
	public boolean isRoot() {
		return !hasParent();
	}

	/**
	 * Возвращает признак абстрактного объекта
	 * @return 
	 */
	public final boolean isAbstract() {
		return uuid == null;
	}
	
	public final boolean isDirectory() {
		return is_folder;
	}
	
	/**
	 * Возвращает наличие родителя
	 * @return 
	 */
	public boolean hasParent() {
		return parent != null;
	}

	/**
	 * Возвращает родителя объекта
	 * @return 
	 */
	public MDEntry getParent() {
		return parent;
	}
	
	/**
	 * Устанавливает родителя элемента
	 * @param p 
	 */
	public void setParent(MDEntry p) {
		parent = p;
	}
	
	/**
	 * Возвращает корневой словарь метаданных
	 * @return 
	 */
	public Tree<String> getDictionary() {
		if (isRoot()) {
			return dictionary;
		} else {
			return getParent().getDictionary();
		}
	}
	
	/**
	 * Возвращает дерево ссылок на элементы хранилища
	 * @return 
	 */
	public final Tree<EntryLink> getStorageLinks() {
		if (isRoot()) {
			return storage_links;
		} else {
			return getParent().getStorageLinks();
		}
	}
	
	/**
	 * Возвращает ссылку на элемент хранилища
	 * @return 
	 */
	public final EntryLink getStorageLink() {
		return storage_link;
	}
	
	/**
	 * Возвращает корневую страницу хранилища
	 * @return 
	 */
	public Page getStoragePage() {
		if (isRoot()) {
			return storage_page;
		} else {
			return getParent().getStoragePage();
		}
	}
	
	/**
	 * Возвращает элемент хранилища по uuid элемента
	 * @return 
	 * @throws java.io.IOException 
	 */
	public final Entry getStorageEntry() throws IOException {
		return getStorageEntryReader().read(storage_link);
	}
	
	/**
	 * Возвращает поток чтения из элемента
	 * @return
	 * @throws IOException 
	 */
	public InputStream getInputStream() throws IOException {
		return null;
	}
	
	/**
	 * Возвращает объект буферизованного чтения их хранилища
	 * @return 
	 */
	public final BufferedEntryReader getStorageEntryReader() {
		if (isRoot()) {
			return entry_reader;
		} else {
			return getParent().getStorageEntryReader();
		}
	}
	
	/**
	 * Заполняет строковое представление элемента
	 * @param b
	 * @param e
	 * @param prefix 
	 */
	private void fillToString(StringBuilder b, MDEntry e, String prefix) {
		
		b.append(prefix);
		b.append(e.getName()).append(":").append(e.getUUID());
		
		Iterator<MDEntry> i = e.getEntries();
		while (i.hasNext()) {
			b.append("\n");
			fillToString(b, i.next(), prefix.concat("    "));
		}
	}
	
	/**
	 * Возвращает строковое представление элемента
	 * @return 
	 */
	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		fillToString(b, this, "");
		return b.toString();
	}
}
