/*
 * Copyright 2015 shmalevoz@gmail.com (Valeriy Krynin).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ru.shmalevoz.v8cf;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Logger;
import ru.shmalevoz.utils.GetOpt;
import ru.shmalevoz.utils.IO;
import ru.shmalevoz.utils.Log;
import ru.shmalevoz.v8cf.IO.DataOutput;
import ru.shmalevoz.v8cf.metadata.v8Metadata;
import ru.shmalevoz.v8cf.storage.Container;

/**
 *
 * @author shmalevoz
 */
public class Main {
	
	private static final Logger log = ru.shmalevoz.utils.Log.getLogger(Main.class.getName());

	/**
	 * @param args the command line arguments
	 * @throws java.text.ParseException
	 */
	public static void main(String[] args) throws ParseException {
		
		// Сразу устанавливаем кодовую страницу стандартных потоков
		IO.setStdCodepage();
		
		GetOpt opts = new GetOpt(ru.shmalevoz.utils.IO.getJarName(new Main()));
		opts.addSpec("f", GetOpt.REQUIRED_OPTION, GetOpt.REQUIRED_ARGUMENT, "Файл конфигурации 1C 8.x", "file");
		opts.addSpec("d", GetOpt.REQUIRED_OPTION, GetOpt.REQUIRED_ARGUMENT, "Каталог хранения", "directory");
		opts.addSpec("r", GetOpt.OPTIONAL_OPTION, GetOpt.NO_ARGUMENT, "Создавать корневой каталог с именем вида обрабатываемого файла", "create-root");
		opts.addSpec("s", GetOpt.OPTIONAL_OPTION, GetOpt.NO_ARGUMENT, "Записывать необработанное содержимое файла конфигурации в каталог sources", "sources");
		opts.addSpec("u", GetOpt.OPTIONAL_OPTION, GetOpt.NO_ARGUMENT, "Записывать данные о необработанных элементах к каталог unparsed", "unparsed");
		opts.addSpec("v", GetOpt.OPTIONAL_OPTION, GetOpt.REQUIRED_ARGUMENT, "Уровень вывода сообщений (0-нет, 8-все. По-умолчанию 2)", "verbose");
		opts.addSpec("h", GetOpt.OPTIONAL_OPTION, GetOpt.NO_ARGUMENT, "Печатать помощь", "help");
		
		if (!opts.parse(args)) {
			System.out.println("Декомпиляция файла конфигурации 1С Предприятия 8.x");
			System.out.println(opts.getOptHelp());
			if (!opts.presentOpt("h")) {
				log.warning(opts.getError());
				System.exit(1);
			}
			System.exit(0);
		}
		if (opts.presentOpt("v")) {
			String level = opts.getOptValue("v");
			if (level != null) {
				Log.setDefaultLevel(level);
			} else {
				Log.setDefaultLevel(Log.LEVEL_VERBOSE_DEFAULT);
			}
			Log.setLevel(log, Log.LEVEL_DEFAULT);
		}
		// Проверяем доступность каталога 
		File parent = new File(opts.getOptValue("d"));
		if (!parent.exists()) {
			log.severe("Не удалось найти каталог " + opts.getOptValue("d"));
			System.exit(1);
		}
		if (!parent.isDirectory()) {
			log.severe("Указанный путь " + opts.getOptValue("d") + " не является каталогом!");
			System.exit(1);
		}
		
		String filename = opts.getOptValue("f");
		boolean createSources = opts.presentOpt("s");
		boolean createUnparsed = opts.presentOpt("u");
		boolean create_root = opts.presentOpt("r");
		
		try {
			
			v8Metadata v = new v8Metadata(filename);
			
			DataOutput out = new DataOutput(create_root);
			out.write(v, parent, createSources, createUnparsed);
		} catch (IOException e) {
			log.severe(e.getMessage());
			System.exit(1);
		}
		
	}
}
